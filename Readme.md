# Hbase

This is a Hbase Ruby API Client

## Installation

If you're using bundler, add the following to the Gemfile

    gem 'hbase-client'

otherwise install the gem as normal

    gem install hbase-client

You need to require and configure Hbase inside your application

    require 'hbase-client'

    HbaseClient.configure(
      logger: Logger.new($stdout),
      host: 'localhost',
      port: 9090
    )

## Usage

Create a client, then execute commands

    client = HbaseClient::Client.new

    client.exec(:getTableNames)

    client.mutli_exec do |conn|
      conn.getTableNames
    end
