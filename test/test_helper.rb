require 'yaml'
require 'test/unit'
require 'bundler/setup'
Bundler.require

ROOT = File.expand_path(File.dirname(__FILE__))

require_relative File.join(ROOT, %w{.. lib  hbase-client})

CONFIG = YAML.load_file(File.join(ROOT, %w{fixtures config.yml}))

HbaseClient.configure(
  logger: Logger.new($stdout)
)