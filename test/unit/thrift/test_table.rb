require_relative '../../test_helper'

class TestTable < Test::Unit::TestCase

  def setup
    @client = HbaseClient::Client.new(
      host: CONFIG['server']['host'],
      port: CONFIG['server']['port']
    )
  end

  def test_list_tables
    assert_nothing_raised do
      @client.multi_exec do |conn|
        conn.getTableNames
      end
    end

    assert_nothing_raised do
      @client.exec(:getTableNames)
    end
  end

end