require File.join(File.expand_path('../lib', __FILE__), 'version')

Gem::Specification.new do |gem|
  gem.name          = "hbase-client"
  gem.version       = HbaseClient::VERSION
  gem.authors       = ["Daniel Padden"]
  gem.email         = ["hypernova2002@gmail.com"]
  gem.description   = %q{Hbase API Client}
  gem.summary       = %q{Connect to the hbase api interface}
  gem.homepage      = ""

  gem.files         = `git ls-files -z`.split("\x0")
  gem.executables   = gem.files.grep(%r{^bin/}) { |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_dependency 'thrift', "0.9.1"
  gem.add_dependency 'rack'
  gem.add_dependency 'thin'

  gem.add_development_dependency 'bundler', '>= 1.3'
end
