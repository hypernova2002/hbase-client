module HbaseClient

  def configure(opts = {})
    default_configuration.each do |key, value|
      config[key] = opts[key] || value
    end
  end

  def config
    @@config ||= {}
  end

  private

    def default_configuration
      {
        logger: Logger.new(nil),
        adapter: Adapters::THRIFT,
        host: 'localhost',
        port: 9090,
        transport: ::Thrift::BufferedTransport,
        protocol: ::Thrift::BinaryProtocol
      }
    end

    module_function :configure, :default_configuration, :config
end