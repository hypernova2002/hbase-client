module HbaseClient
  class Client

    attr_reader :client, :logger

    def initialize(opts = {})
      host = opts[:host] || HbaseClient.config[:host]
      port = opts[:port] || HbaseClient.config[:port]

      @logger = opts[:logger] || HbaseClient.config[:logger]
      @socket = ::Thrift::Socket.new(host, port)
      @transport = (opts[:transport] || HbaseClient.config[:transport]).new(@socket)
      @protocol = (opts[:protocol] || HbaseClient.config[:protocol]).new(@transport)
      @client = HbaseClient::Thrift::Hbase::Client.new(@protocol)

      @logger.info "Connected to Hbase Thrift server #{host}:#{port}"
    end

    def open
      @transport.open
    end

    def close
      @transport.close
    end

    def exec(method, *args)
      begin
        open
        @logger.info "Executing command #{method.to_s} with args: #{args.join(",")}"
        @client.send(method, *args)
      rescue => e
        @logger.error "#{e.message}"
        raise e
      ensure
        close
      end
    end

    def multi_exec
      begin
        open
        yield @client
      rescue => e
        @logger.error "#{e.message}"
        raise e
      ensure
        close
      end
    end

  end
end